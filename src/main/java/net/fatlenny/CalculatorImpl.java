package net.fatlenny;

public class CalculatorImpl implements Calculator {
    private int result;

    public void add(int a, int b) {
        result = a + b;
    }

    public void multiply(int a, int b) {
        result = a * b;
    }

    public void subtract(int a, int b) {
        result = b - a;
    }

    public int getResult() {
        return result;
    }

}
