package net.fatlenny;

public interface Calculator {
    void add(int a, int b);

    void multiply(int a, int b);

    void subtract(int a, int b);

    int getResult();
}
