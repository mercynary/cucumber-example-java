package net.fatlenny;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CalculatorSteps {
    private Calculator calculator;

    @Before
    public void setUp() {
        calculator = new CalculatorImpl();
    }

    @Given("^I have a calculator$")
    public void i_have_a_calculator() throws Throwable {
        assertNotNull(calculator);
    }

    @When("^I add (\\d+) and (\\d+)$")
    public void i_add_and(int a, int b) throws Throwable {
        calculator.add(a, b);
    }

    @When("^I multiply (\\d+) with (\\d+)$")
    public void i_multiply_with(int a, int b) throws Throwable {
        calculator.multiply(a, b);
    }

    @When("^I subtract (\\d+) from (\\d+)$")
    public void i_subtract_from(int a, int b) throws Throwable {
        calculator.subtract(a, b);
    }

    @Then("^the result should be (\\d+)$")
    public void the_result_should_be(int result) throws Throwable {
        assertEquals(result, calculator.getResult());
    }
}
