package net.fatlenny;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        format = { "pretty", "html:target/cucumber", "json:target/cucumber/cucumber.json" },
        glue = "net.fatlenny",
        features = "classpath:cucumber/calculator.feature")
public class RunCalculatorTest {
}
